import tensorflow as tf
from tensorflow import keras
import numpy as np

def convolution_encoding(input_img, filter_dim = 16, window = (3,3), pool= (2,2)):
    """Creates a convolutional encoding of an image

    Parameters
    ----------
    input_img : tensor
        keras tensor representing an image
    filter_dim : integer
        The dimensionality of the output space. # of output filters in the
        convolution
    window : integer or tuple/list of 2 integers
        specifying the strides of the convolution. Can be a single integer to
        specify the same value for all spatial dimensions
    pool : integer or tuple of 2 integers
        factors by which to downscale (vertical, horizontal).
        (2, 2) will halve the input in both spatial dimension.
        If only one integer is specified, the same window length
        will be used for both dimensions.

    Returns
    -------
    Keras layers
        Keras layers of convolutional encoding for an image

    """
    conv_encoded_1 = keras.layers.Conv2D(filter_dim, window, activation='relu', padding='same')(input_img)
    dim_reduction_1 = keras.layers.MaxPooling2D(pool, padding='same')(conv_encoded_1)
    conv_encoded_2 = keras.layers.Conv2D(int(filter_dim/2), window, activation='relu', padding='same')(dim_reduction_1)
    dim_reduction_2 = keras.layers.MaxPooling2D(pool, padding='same')(conv_encoded_2)
    conv_encoded_3 = keras.layers.Conv2D(int(filter_dim/2), window, activation='relu', padding='same')(dim_reduction_2)
    dim_reduction_3 = keras.layers.MaxPooling2D(pool, padding='same')(conv_encoded_3)
    dim = np.prod(dim_reduction_3.shape.as_list()[1:])
    flattened = keras.layers.Reshape((dim,))(dim_reduction_3)
    dense_1 = keras.layers.Dense(dim // 2, activation = 'relu')(flattened)
    dense_2 = keras.layers.Dense(dim // 4, activation = None)(dense_1)
    return keras.layers.Lambda(lambda x: tf.math.l2_normalize(x, axis=1))(dense_2)

def build_conv_embed(img_size=(28,28), name=None):
    """builds a convolutional encoding model from standard image input size

    Args:
        img_size (tuple, optional): _description_. Defaults to (28,28).
        name (_type_, optional): _description_. Defaults to None.

    Returns:
        _type_: Keras/Tensorflow model
    """
    input_img = keras.Input(shape=(*img_size,1))
    return keras.Model(input_img, convolution_encoding(input_img), name=name)